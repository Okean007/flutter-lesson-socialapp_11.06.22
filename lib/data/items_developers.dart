import '../screens/socialAppScreen/constants/assetsApp.dart';
import 'developers_data.dart';

List<Developers> itemDeveloper = [
  Developers('Corey Seorde', AppAssets.images.image1),
  Developers('Ahmad Vetrovs', AppAssets.images.image2),
  Developers('Cristofer Workman', AppAssets.images.image3),
  Developers('Tiana Korsgaard', AppAssets.images.image4),
];
