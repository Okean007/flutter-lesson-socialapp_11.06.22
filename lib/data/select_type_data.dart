abstract class SelectTypeData {
  static List<String> selectTypes = [
    'Black',
    'Bold',
    'Medium',
    'Regular',
    'Light',
    'Rock',
  ];
}
