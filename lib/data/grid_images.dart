import '../screens/socialAppScreen/constants/assetsApp.dart';

List<String> gridImages = [
  AppAssets.images.image5,
  AppAssets.images.image6,
  AppAssets.images.image7,
  AppAssets.images.image8,
  AppAssets.images.image9,
  AppAssets.images.image10,
  AppAssets.images.image11,
  AppAssets.images.image12,
  AppAssets.images.image13,
];
