import 'package:flutter/material.dart';
import 'package:socialproject/screens/socialAppScreen/widgets/appbar_widget.dart';
import 'package:socialproject/screens/socialAppScreen/widgets/avatar_stac_widget.dart';
import 'package:socialproject/screens/socialAppScreen/widgets/bottom_add.dart';
import 'package:socialproject/screens/socialAppScreen/widgets/bottom_section.dart';
import 'package:socialproject/screens/socialAppScreen/widgets/custom_divider.dart';
import 'package:socialproject/screens/socialAppScreen/widgets/grid_view_widget.dart';
import 'package:socialproject/screens/socialAppScreen/widgets/list_view_widget.dart';
import 'package:socialproject/screens/socialAppScreen/widgets/selecte_type_listView.dart';
import 'package:socialproject/screens/socialAppScreen/widgets/text_widget.dart';

class SocialApp extends StatelessWidget {
  const SocialApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: const AppBarr(),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(
              height: 18,
            ),
            const StacAvatar(),
            const SizedBox(
              height: 24,
            ),
            const CustomDivider(),
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 12, horizontal: 16),
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: TextSocial(title: 'Select type')),
            ),
            const SelecTypeSection(),
            const SizedBox(
              height: 16,
            ),
            const CustomDivider(),
            Padding(
              padding: const EdgeInsets.all(14.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const [
                  TextSocial(title: 'Friends'),
                  SizedBox(
                    height: 2,
                  ),
                  ListViewWidget(),
                  AddFriends(),
                  SizedBox(
                    height: 16,
                  ),
                  CustomDivider(
                    indent: 0,
                    endIndent: 0,
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  TextSocial(title: 'My media'),
                  SizedBox(
                    height: 16,
                  ),
                  GridViewWidget(),
                  SizedBox(
                    height: 16,
                  ),
                  BottonSection(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
