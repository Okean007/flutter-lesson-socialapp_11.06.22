// ignore_for_file: file_names

import 'package:flutter/material.dart';

import '../../../data/select_type_data.dart';
import 'box_widger.dart';

class SelecTypeSection extends StatefulWidget {
  const SelecTypeSection({Key? key}) : super(key: key);
  @override
  State<SelecTypeSection> createState() => _SelecTypeSectionState();
}

class _SelecTypeSectionState extends State<SelecTypeSection> {
  int selectIndex = 2;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 32,
      child: ListView.separated(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          scrollDirection: Axis.horizontal,
          separatorBuilder: ((context, index) => const SizedBox(width: 8)),
          itemBuilder: (context, index) => BoxWidget(
                isSelected: selectIndex == index,
                onTap: () {
                  selectIndex = index;
                  setState(() {});
                },
                title: SelectTypeData.selectTypes[index],
              ),
          itemCount: SelectTypeData.selectTypes.length),
    );
  }
}
