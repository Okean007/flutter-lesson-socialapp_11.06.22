import 'package:flutter/material.dart';

class BoxWidget extends StatelessWidget {
  const BoxWidget(
      {Key? key,
      required this.title,
      required this.isSelected,
      required this.onTap})
      : super(key: key);
  final String title;
  // ignore: prefer_typing_uninitialized_variables
  final isSelected;
  final Function() onTap;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          color: isSelected ? const Color(0xffF2E7FE) : Colors.grey.shade300,
          border: isSelected
              ? Border.all(
                  color: const Color(0xffDBB2FF),
                  width: 1,
                )
              : null,
          borderRadius: BorderRadius.circular(68),
        ),
        padding: const EdgeInsets.symmetric(
          vertical: 6,
          horizontal: 12,
        ),
        child: Text(
          title,
          style: TextStyle(
            fontSize: 14,
            color: isSelected
                ? const Color(0xff6200EE)
                : const Color(0xff000000).withOpacity(0.38),
          ),
        ),
      ),
    );
  }
}
