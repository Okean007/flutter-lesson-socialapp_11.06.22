import 'package:flutter/material.dart';

class CustomDivider extends StatelessWidget {
  const CustomDivider({Key? key, this.indent = 18, this.endIndent = 18})
      : super(key: key);
  final double indent;
  final double endIndent;
  @override
  Widget build(BuildContext context) {
    return Divider(
      color: const Color(0xff212121).withOpacity(0.9),
      indent: indent,
      endIndent: endIndent,
      height: 1,
    );
  }
}
