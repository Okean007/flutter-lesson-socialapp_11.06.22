import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:socialproject/data/grid_images.dart';

import '../constants/assetsApp.dart';

class GridViewWidget extends StatelessWidget {
  const GridViewWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 370,
      child: GridView.builder(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          mainAxisSpacing: 5,
          crossAxisSpacing: 5,
        ),
        itemBuilder: (context, index) => Stack(
          children: [
            Image.asset(
              gridImages[index],
              width: double.infinity,
              height: double.infinity,
              fit: BoxFit.fill,
            ),
            Positioned(
              top: 10,
              right: 14,
              child: Container(
                padding: const EdgeInsets.all(8),
                decoration: BoxDecoration(
                  color: const Color(0xffCF6679),
                  shape: BoxShape.circle,
                  border: Border.all(
                    width: 1.5,
                    color: Colors.white,
                  ),
                ),
                child: SizedBox(
                  height: 8,
                  width: 8,
                  child: SvgPicture.asset(
                    AppAssets.svg.closeReed,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
        itemCount: gridImages.length,
      ),
    );
  }
}
