import 'package:flutter/material.dart';

class DeleteAddButton extends StatelessWidget {
  const DeleteAddButton(
      {Key? key,
      required this.isActive,
      required this.title,
      required this.onTap})
      : super(key: key);
  final bool isActive;
  final String title;
  final Function() onTap;
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: SizedBox(
        height: 40,
        child: ElevatedButton(
          onPressed: onTap,
          style: isActive
              ? ElevatedButton.styleFrom(
                  primary: const Color(0xff6200EE),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8)),
                )
              : ElevatedButton.styleFrom(
                  primary: Colors.white,
                  elevation: 0,
                  side: BorderSide(
                    width: 1,
                    color: const Color(0xff000000).withOpacity(0.12),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8)),
                ),
          child: Text(
            title.toUpperCase(),
            style: TextStyle(
              fontWeight: FontWeight.w500,
              letterSpacing: 1.25,
              color: isActive ? null : const Color(0xff6200EE),
            ),
          ),
        ),
      ),
    );
  }
}
