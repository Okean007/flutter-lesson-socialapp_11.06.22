import 'package:flutter/material.dart';

import 'eleveted_bottom.dart';

class BottonSection extends StatefulWidget {
  const BottonSection({Key? key}) : super(key: key);

  @override
  State<BottonSection> createState() => _ButtonSectionState();
}

class _ButtonSectionState extends State<BottonSection> {
  int index = 1;
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        DeleteAddButton(
          isActive: index == 1,
          title: 'Delete',
          onTap: () {
            index = 1;
            setState(() {});
          },
        ),
        const SizedBox(
          width: 16,
        ),
        DeleteAddButton(
          isActive: index == 2,
          title: 'Add',
          onTap: () {
            index = 2;
            setState(() {});
          },
        ),
      ],
    );
  }
}
