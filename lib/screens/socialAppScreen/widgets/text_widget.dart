import 'package:flutter/material.dart';
class TextSocial extends StatelessWidget {
  const TextSocial({
    Key? key,
    required this.title,
  }) : super(key: key);
  final String title;

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: const TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.w500,
        letterSpacing: 0.44,
      ),
    );
  }
}