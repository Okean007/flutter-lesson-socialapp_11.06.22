import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../data/developers_data.dart';
import '../constants/assetsApp.dart';

class Friends extends StatelessWidget {
  const Friends({Key? key, required this.developer}) : super(key: key);
  final Developers developer;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16),
      child: Row(
        children: [
          CircleAvatar(
            radius: 20,
            child: Image.asset(developer.avatar),
          ),
          const SizedBox(
            width: 16,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                developer.name,
                style: const TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
              Text(
                'developer',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  color: Colors.black.withOpacity(0.6),
                ),
              ),
            ],
          ),
          const Spacer(),
          IconButton(
            onPressed: () {},
            icon: SvgPicture.asset(AppAssets.svg.closeReed),
          ),
        ],
      ),
    );
  }
}
