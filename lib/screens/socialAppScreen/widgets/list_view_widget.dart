import 'package:flutter/material.dart';
import 'package:socialproject/data/items_developers.dart';
import 'custom_divider.dart';
import 'developer_friends.dart';

class ListViewWidget extends StatelessWidget {
  const ListViewWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      scrollDirection: Axis.vertical,
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      separatorBuilder: (BuildContext context, int index) {
        return CustomDivider(
          indent: MediaQuery.of(context).size.width * 0.17,
        );
      },
      itemCount: itemDeveloper.length,
      itemBuilder: (buildContext, int index) {
        return Friends(
          developer: itemDeveloper[index],
        );
      },
    );
  }
}
