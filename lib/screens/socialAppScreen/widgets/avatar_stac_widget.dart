import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../constants/assetsApp.dart';

class StacAvatar extends StatelessWidget {
  const StacAvatar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Stack(
          children: [
            CircleAvatar(
              radius: 59,
              child: Image.asset(AppAssets.images.avatar),
            ),
            Positioned(
              bottom: 0,
              right: 0,
              child: Container(
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: const Color(0xff6200ee),
                  border: Border.all(width: 2, color: Colors.white),
                ),
                child: SvgPicture.asset(
                  AppAssets.svg.plusWhite,
                  height: 16,
                  width: 16,
                ),
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 16,
        ),
        const Text(
          'Tiana Rosser',
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w500,
            letterSpacing: 0.20,
          ),
        ),
        const Text(
          'Developer',
          style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w500,
              color: Color(0xf6666666),
              letterSpacing: 0.5),
        ),
      ],
    );
  }
}
