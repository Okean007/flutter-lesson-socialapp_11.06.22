// ignore_for_file: file_names

abstract class AppAssets {
  static const images = _Images();
  static const svg = _Svg();
}

class _Images {
  const _Images();
  final String avatar = 'assets/images/bitmap/avatar.png';
  final String image1 = 'assets/images/bitmap/image1.png';
  final String image2 = 'assets/images/bitmap/image2.png';
  final String image3 = 'assets/images/bitmap/image3.png';
  final String image4 = 'assets/images/bitmap/image4.png';
  final String image5 = 'assets/images/bitmap/image5.png';
  final String image6 = 'assets/images/bitmap/image6.png';
  final String image7 = 'assets/images/bitmap/image7.png';
  final String image8 = 'assets/images/bitmap/image8.png';
  final String image9 = 'assets/images/bitmap/image9.png';
  final String image10 = 'assets/images/bitmap/image10.png';
  final String image11 = 'assets/images/bitmap/image11.png';
  final String image12 = 'assets/images/bitmap/image12.png';
  final String image13 = 'assets/images/bitmap/image13.png';
}

class _Svg {
  const _Svg();
  final String arrowBack = 'assets/images/svg/arrowBack.svg';
  final String closeReed = 'assets/images/svg/closeReed.svg';
  final String closeWhite = 'assets/images/svg/closeWhite.svg';
  final String plusBlack = 'assets/images/svg/plusBlack.svg';
  final String plusWhite = 'assets/images/svg/plusWhite.svg';
  final String tree = 'assets/images/svg/tree.svg';
}
